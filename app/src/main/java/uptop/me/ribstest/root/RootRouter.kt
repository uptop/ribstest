package uptop.me.ribstest.root

import com.uber.rib.core.ViewRouter
import uptop.me.ribstest.logged_out.LoggedOutBuilder
import uptop.me.ribstest.logged_out.LoggedOutRouter

/**
 * Adds and removes children of {@link RootBuilder.RootScope}.
 *
 * TODO describe the possible child configurations of this scope.
 */
class RootRouter(
    view: RootView,
    interactor: RootInteractor,
    component: RootBuilder.Component,
    private val loggedOutBuilder: LoggedOutBuilder) : ViewRouter<RootView, RootInteractor, RootBuilder.Component>(view, interactor, component) {

    private var loggedOutRouter: LoggedOutRouter? = null

    internal fun attachLoggedOut() {
        loggedOutRouter = loggedOutBuilder.build(view)
        print("this!!! $loggedOutRouter  +    $loggedOutBuilder    +     ${loggedOutRouter!!.view}")
        attachChild(loggedOutRouter!!)
        view.addView(loggedOutRouter!!.view)
    }

    internal fun detachLoggedOut() {
        if (loggedOutRouter != null) {
            detachChild(loggedOutRouter!!)
            view.removeView(loggedOutRouter!!.view)
            loggedOutRouter = null
        }
    }

    internal fun attachLoggedIn() {
//        attachChild(loggedInBuilder.build())
    }
}
