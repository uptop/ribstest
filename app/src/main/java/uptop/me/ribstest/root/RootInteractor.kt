package uptop.me.ribstest.root

import com.uber.rib.core.Bundle
import com.uber.rib.core.Interactor
import com.uber.rib.core.RibInteractor
import javax.inject.Inject

/**
 * Coordinates Business Logic for [RootScope].
 *
 * TODO describe the logic of this scope.
 */
@RibInteractor
class RootInteractor : Interactor<RootInteractor.RootPresenter, RootRouter>() {

  @Inject
  lateinit var presenter: RootPresenter

  override fun didBecomeActive(savedInstanceState: Bundle?) {
    print("didBecomeActive")
    super.didBecomeActive(savedInstanceState)
    router.attachLoggedOut()
  }

//  internal inner class LoggedOutListener : LoggedOutInteractor.Listener {
//    override fun login(userNameA: String?, userNameB: String?) {
//      router.detachLoggedOut()
//      router.attachLoggedIn()
//    }
//  }

  /**
   * Presenter interface implemented by this RIB's view.
   */
  interface RootPresenter
}
