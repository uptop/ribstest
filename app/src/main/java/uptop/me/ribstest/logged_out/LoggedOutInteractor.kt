package uptop.me.ribstest.logged_out

import android.support.v4.util.Pair
import com.uber.rib.core.Bundle
import com.uber.rib.core.Interactor
import com.uber.rib.core.RibInteractor
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Coordinates Business Logic for [LoggedOutScope].
 *
 * TODO describe the logic of this scope.
 */
@RibInteractor
class LoggedOutInteractor : Interactor<LoggedOutInteractor.LoggedOutPresenter, LoggedOutRouter>() {
//
//  @Inject
//  internal var listener: Listener
  @Inject
  lateinit var presenter: LoggedOutPresenter

  override fun didBecomeActive(savedInstanceState: Bundle?) {
    super.didBecomeActive(savedInstanceState)
//    presenter
//            .loginName()
//            .subscribe({ names ->
//              if (!isEmpty(names.first) && !isEmpty(names.second)) {
//                listener.login(names.first, names.second)
//              }
//            })
  }

  /**
   * Presenter interface implemented by this RIB's view.
   */
  interface LoggedOutPresenter {
    fun loginName(): Observable<Pair<String, String>>
  }

  interface Listener {
    fun login(userNameA: String?, userNameB: String?)
  }
}
