package uptop.me.ribstest.logged_out

import android.content.Context
import android.support.v4.util.Pair
import android.util.AttributeSet
import android.widget.LinearLayout
import io.reactivex.Observable

/**
 * Top level view for {@link LoggedOutBuilder.LoggedOutScope}.
 */
class LoggedOutView : LinearLayout, LoggedOutInteractor.LoggedOutPresenter {
    override fun loginName(): Observable<Pair<String, String>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?): this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle)
}
