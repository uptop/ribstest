package uptop.me.ribstest

import android.view.ViewGroup
import com.uber.rib.core.RibActivity
import com.uber.rib.core.ViewRouter
import uptop.me.ribstest.root.RootBuilder

class MainActivity : RibActivity() {

    @SuppressWarnings("unchecked")
    override fun createRouter(parentViewGroup: ViewGroup?): ViewRouter<*, *, *> {
        val rootBuilder = RootBuilder(object : RootBuilder.ParentComponent {})
        return rootBuilder.build(parentViewGroup!!)
    }
}
