package uptop.me.ribstest.logged_in

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout

/**
 * Top level view for {@link LoggedInBuilder.LoggedInScope}.
 */
class LoggedInView : LinearLayout, LoggedInInteractor.LoggedInPresenter {
    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?): this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle)
}
